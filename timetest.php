#!/usr/bin/php
<?php
// Dirty Ansible hacks
// WANT_JSON

class AnsibleModule {
    private static $arguments = array();
    private static $return_vars = array(
        'changed' => 'false',
        'time' => '',
        'counter' => 0,
    );

    public static function execute(string $arguments_file)
    {
        // Following depends on WANT_JSON
        if (!self::$arguments = json_decode(file_get_contents($arguments_file), true)) {
            self::error("Couldn't parse input arguments");
        }

        // Primary logic
        if(is_numeric(self::$arguments['counter'])) {
            self::$return_vars['counter'] += self::$arguments['counter'];
        } else {
            self::error("Input argument 'counter' must be numeric");
        }

        self::$return_vars['changed'] = "True";
        self::$return_vars['time'] = date("H:i:s", time());

        // Return successful payload
        echo json_encode(self::$return_vars);
    }

    # Call error handler if any issues in processing
    private static function error(string $msg='Failed')
    {
        echo json_encode(array(
            'failed' => "True",
            'msg' => $msg
            ));

        exit(1);
    }

}

AnsibleModule::execute($argv[1]);
